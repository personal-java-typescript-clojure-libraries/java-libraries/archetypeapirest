module org.twbillot.ArchetypeAPIREST {
    requires java.sql;
    requires java.persistence;
    requires java.validation;
    requires com.fasterxml.jackson.annotation;
    requires org.hibernate.orm.core;
    requires org.hibernate.validator;
    requires spring.data.commons;
}