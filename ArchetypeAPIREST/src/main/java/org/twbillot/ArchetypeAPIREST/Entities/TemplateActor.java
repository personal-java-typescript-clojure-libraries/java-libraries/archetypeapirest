package org.twbillot.ArchetypeAPIREST.Entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotBlank;
import org.hibernate.validator.constraints.Length;

@MappedSuperclass
@JsonIgnoreProperties(
        value = { "createdAt", "updatedAt", "deletedAt", "updatedBy" },
        allowGetters = true)
public abstract class TemplateActor extends TemplateEntity {
    // // // // // ATTRIBUTES
    // Serial number
    public	static	final	long		serialVersionUID	= 10000L;
    // Table Columns:
    // Data Columns
    @NotBlank
    @Length(max = shortMax)
    private					String		name;
    // // // // // CONSTRUCTORS
    public TemplateActor() { super(); } // For Dev
    public TemplateActor(TemplateActor updater) { super(updater); }
    public TemplateActor(TemplateActor updater, String	name) {
        super(updater);
        this.name = truncateShort(name);
    }
    // // // // // GETTERS
    public	String		getName() { return name; }
    // // // // // SETTERS
    public	void		setName(String name) { this.name = truncateShort(name); }
    // // // // // METHODS
    public	void		update(String paramName, String paramValue) {
        if (paramName.equals("name")) { setName(paramValue); }
    }
}
