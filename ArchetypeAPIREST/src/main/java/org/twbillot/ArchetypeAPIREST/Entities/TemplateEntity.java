package org.twbillot.ArchetypeAPIREST.Entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import org.hibernate.annotations.Type;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

@MappedSuperclass
@JsonIgnoreProperties(
        value = { "createdAt", "updatedAt", "deletedAt", "updatedBy" },
        allowGetters = true)
public abstract class TemplateEntity implements Serializable {
    // // // // // ATTRIBUTES
    // Serial number
    public		static	final	long	serialVersionUID	= 10000L;
    // Processing constants
    protected	static	final	int		shortMax			= 42;
    protected	static	final	int		longMax				= 120;
    // Table Columns:
    // Identifier Columns
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected					long	id;
    @NotNull
    @Type(type = "uuid-char")
    @Column(unique = true)
    protected					UUID	uuid;
    // Timestamp Columns
    @Column(nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    protected					Date	createdAt;
    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    protected					Date	updatedAt;
    @Column(nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    protected					Date	deletedAt;
    @Type(type = "uuid-char")
    @Column(nullable = false)
    protected					UUID	updatedBy;
    // // // // // CONSTRUCTORS
    public TemplateEntity() { // For Dev
        this.uuid = assignUUID();
        this.deletedAt = null;
    }
    public TemplateEntity(TemplateActor updater) {
        this.uuid = assignUUID();
        this.deletedAt = null;
        this.updatedBy = updater.getUUID();
    }
    // // // // // GETTERS
    public	final	long	getID() { return id; }
    public	final	UUID	getUUID() { return uuid; }
    public	final	Date	getCreatedAt() { return createdAt; }
    public	final	Date	getUpdatedAt() { return updatedAt; }
    public	final	Date	getDeletedAt() { return deletedAt; }
    public	final	UUID	getUpdatedBy() { return updatedBy; }
    // // // // // SETTERS
    private	final	void	setDeletedAt() { this.deletedAt = nowUTC(); }
    private	final	void	setUpdatedBy(TemplateActor updater) { this.updatedBy = updater.getUUID(); }
    // // // // // METHODS
    private		static	final Timestamp nowUTC() {
        LocalDateTime ldt = LocalDateTime.now();
        ldt.atOffset(ZoneOffset.UTC);
        Timestamp ts = Timestamp.valueOf(ldt);
        return ts;
    }
    protected	static	final	UUID		assignUUID() { return UUID.randomUUID(); }
    protected	static	final	String		truncateShort(String value) {
        String output = value;
        if (shortMax < output.length()) {
            output = value.substring(0, shortMax);
        }
        return output;
    }
    protected	static	final	String		truncateLong(String value) {
        String output = value;
        if (longMax < output.length()) {
            output = value.substring(0, longMax);
        }
        return output;
    }
    protected	static	final	Timestamp	parseTimeStamp(String value) {
        LocalDateTime ldt = LocalDateTime.parse(value);
        ldt.atOffset(ZoneOffset.UTC);
        Timestamp ts = Timestamp.valueOf(ldt);
        return ts;
    }
    protected	static	final	int			parseInt(String value) {
        int output = -1;
        try { output = Integer.parseInt(value);
        } catch (NumberFormatException e) {
            System.out.println(e + " with " + value);
            //Logger.log(e + " with " + value);
        }
        return output;
    }
    public		final	void	delete() { setDeletedAt(); }
    public		final	void	attributeTo(TemplateActor actor) { setUpdatedBy(actor); }
    private	    final	void	initialize() {
        this.uuid = assignUUID();
        this.deletedAt = null;
    }
    public      final	void    initialize(TemplateActor actor) {
        initialize();
        setUpdatedBy(actor);
    }
    protected   final   void    fit(
            long	id,
            UUID    uuid,
            Date    createdAt,
            Date	updatedAt,
            Date	deletedAt,
            UUID	updatedBy) {
        this.id = id;
        this.uuid = uuid;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.deletedAt = deletedAt;
        this.updatedBy = updatedBy;
    }
    public		final	String  generateTestUUID() { // For Dev
        return UUID.randomUUID().toString();
    }
}
