package org.twbillot.ArchetypeAPIREST.Entities;

import javax.persistence.Column;
import java.io.Serializable;

public abstract class TemplateEmbeddable<T extends TemplateEntity> implements Serializable {
    // Attributes
    // Serial number
    public	static	final	long		serialVersionUID	= 10000L;
    @Column(name = "parent_id", nullable = false, updatable = false)
    private	long        parentID;
    @Column(name = "parent_class", nullable = false, updatable = false)
    private	Class<T>    parentClass;
    // Constructors
    public TemplateEmbeddable(T parent) { setParent(parent); }
    // Getters
    public	long        getParentID() { return parentID; }
    public	Class<T>    getParentClass() { return parentClass; }
    // Setters
    public	void	setParentID(long parentID) { this.parentID = parentID; }
    public	void	setParentID(T parent) { this.parentID = parent.getID(); }
    public	void	setParentClass(Class<T> parentClass) { this.parentClass = parentClass; }
    public	void	setParentClass(T parent) { this.parentClass = (Class<T>) parent.getClass(); }
    public void     setParent(T parent) { setParentID(parent); setParentClass(parent); }
    // Overridden methods
    @Override
    public	boolean	equals(
            Object	other) {
        if (other == null) { return false; }
        if (other == this) { return true; }
        if (other instanceof TemplateEmbeddable) {
            TemplateEmbeddable<T> peOther = (TemplateEmbeddable<T>) other;
           return peOther.parentID == parentID
                   && peOther.parentClass.equals(parentClass);
        }
        return false;
    }
}
