package org.twbillot.ArchetypeAPIREST.Entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
import org.hibernate.sql.Template;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;
/*import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;*/

@MappedSuperclass
public abstract class TemplateFile<T, U extends TemplateEntity> extends TemplateEntity /*implements MultiPartable*/ {
    @Embeddable
    public class Parent extends TemplateEmbeddable<U> { public Parent(U parent) { super(parent); } }
    // Attributes
    // Serial number
    public	static	final	long	serialVersionUID	= 10000L;
    // Table columns:
    @Embedded
    private	Parent  parent;
    @Column(nullable = false)
    @Length(max = shortMax)
    private	String	name;
    @Column(nullable = false)
    @Range(min = 1)
    private	int		numberParts;
    @Transient
    private	byte[]	content;
    // Column Joins
    @Transient
    private	Set<T>	    parts;
    // Processing attributes
    /*@Transient
    private	Stringifer	stringifer;*/
    // Constructors
    public TemplateFile() { super(); /*initStringifer();*/ }
    public TemplateFile(
            TemplateActor   updater,
            U	            parent,
            String			name,
            byte[]			content) {
        super(updater);
        setParent(parent);
        setName(name);
        setContent(content);
        //initStringifer();
    }
    public TemplateFile(
            TemplateActor	updater,
            U               parent,
            String			name,
            Set<T>			parts) {
        super(updater);
        setParent(parent);
        setName(name);
        setParts(parts);
        //initStringifer();
    }
    public TemplateFile(
            TemplateActor	updater,
            U               parent,
            String			name,
            List<T>			parts) {
        super(updater);
        setParent(parent);
        setName(name);
        setParts(parts);
        //initStringifer();
    }
    public TemplateFile(
            TemplateActor   updater,
            U               parent,
            String			name,
            T[]				parts) {
        super(updater);
        setParent(parent);
        setName(name);
        setParts(parts);
        //initStringifer();
    }
    // Getters
    public	Parent          getParent() { return parent; }
    public	String			getName() { return name; }
    public	int				getNumberParts() { return numberParts; }
    public	byte[]			getContent() { return content; }
    public	Set<T>			getParts() { return parts; }
    public	List<T>			getPartsAsList() { return new ArrayList<T>(parts); }
    @SuppressWarnings("unchecked")
    public	T[]				getPartsAsArray() { return (T[]) parts.toArray(); }
    // Setters
    public	void	setParent(Parent entity) { this.parent = entity; }
    public	void	setParent(U entity) { this.parent = new Parent(entity); }
    public	void	setName(String name) { this.name = truncateShort(name); }
    public	void	setNumberParts(int nbrParts) { this.numberParts = nbrParts; }
    public	void	setContent(byte[] content) { this.content = content; }
    public	void	setParts(Set<T> parts) { setNumberParts(parts.size()); this.parts = parts; }
    public	void	setParts(List<T> parts) { setParts(new HashSet<T>(parts)); }
    public	void	setParts(T[] parts) { setParts(new HashSet<T>(Arrays.asList(parts))); }
    // Methods
    private	byte[]	concatParts(
            Set<T>	parts) {
        byte[][] blobs = new byte[parts.size()][];
        int len = 0;
        int idx;
        for (T part : parts) {
            /*idx = part.getKey().getPartIndex();
            blobs[idx] = part.getContent();
            len =+ blobs[idx].length;*/
        }
        byte[] content = new byte[len];
        idx = 0;
        for (byte[] blob : blobs) {
            System.arraycopy(
                    blob, 0,
                    content, idx, blob.length);
            idx =+ blob.length;
        }
        return content;
    }
    private	byte[]	concatParts(List<T> parts) { return concatParts(new HashSet<T>(parts)); }
    @SuppressWarnings("unused")
    private	byte[]	concatParts(T[] parts) { return concatParts(Arrays.asList(parts)); }
    public	void	retrieveContent(List<T> parts) { setParts(parts); setContent(concatParts(parts)); }
    @SuppressWarnings("rawtypes")
    public	boolean	equalsName(
            Object	other) {
        if (other == null) { return false; }
        if (other == this) { return true; }
        if (other instanceof TemplateFile) {
            TemplateFile<T,U> ftOther = (TemplateFile<T,U>) other;
            return ftOther.parent.equals(parent)
                    && ftOther.name.equals(name);
        }
        return false;
    }
    @SuppressWarnings("rawtypes")
    public	boolean	equalsContent(
            Object	other) {
        if (other == null) { return false; }
        if (other == this) { return true; }
        if (other instanceof TemplateFile) {
            TemplateFile<T,U> ftOther = (TemplateFile<T,U>) other;
            return ftOther.parent.equals(parent)
                    && Arrays.equals(ftOther.content, content);
        }
        return false;
    }
    // Overridden methods
    @SuppressWarnings("rawtypes")
    @Override
    public	boolean	equals(
            Object	other) {
        if (other == null) { return false; }
        if (other == this) { return true; }
        if (other instanceof TemplateFile) {
            TemplateFile<T,U> ftOther = (TemplateFile<T,U>) other;
            return ftOther.parent.equals(parent)
                    && ftOther.name.equals(name)
                    && Arrays.equals(ftOther.content, content);
        }
        return false;
    }
    // MultiPartable methods
    // To-Override methods
    /*private	Stringifer	initDefaultStringifer() {
        return new ByteStringifer();
    }
    // Generic methods
    private	void		initStringifer() {
        stringifer = initDefaultStringifer();
    }
    // Override interface methods
    // Override MultiPartable interface methods
    @Override
    public	Stringifer	getDefaultStringifer() {
        return stringifer;
    }
    public	MultiValueMap<String, Serializable>	appendToMap(
            MultiValueMap<String, Serializable>	map,
            String								prefixValue,
            Integer								suffixValue,
            Stringifer							stringifer) {
        String prefix = createPrefix(prefixValue);
        String suffix = createSuffix(suffixValue);
        map = addOneToMap(map, prefix, suffix, "id", getID(), stringifer);
        map = addOneToMap(map, prefix, suffix, "uuid", getUUID(), stringifer);
        map = addOneToMap(map, prefix, suffix, "createdAt", getCreatedAt(), stringifer);
        map = addOneToMap(map, prefix, suffix, "updatedAt", getUpdatedAt(), stringifer);
        map = addOneToMap(map, prefix, suffix, "deletedAt", getDeletedAt(), stringifer);
        map = addOneToMap(map, prefix, suffix, "updatedBy", getUpdatedBy(), stringifer);
        map = addOneToMap(map, prefix, suffix, "name", getName(), stringifer);
        try {
            map.add(
                    "content",
                    stringifer.asString(
                            getContent()));
        } catch (NullPointerException e) {
        }
        return map;
    }
    public	MultiValueMap<String, Serializable>	selfContainedMVMap() {
        MultiValueMap<String, Serializable>	map = new LinkedMultiValueMap<String, Serializable>();
        return appendToMap(map);
    }
    private	Map<String, Serializable>	selfContainedAdd(
            Map<String, Serializable>	map,
            String						key,
            Object						value,
            Stringifer					stringifer) {
        try {
            map.put(
                    key,
                    stringifer.serialize(
                            value));
        } catch (NullPointerException e) {
        }
        return map;
    }
    public	Map<String, Serializable>	selfContainedMap() {
        Map<String, Serializable> map = new HashMap<String, Serializable>();
        Stringifer stringifer = getDefaultStringifer();
        map = selfContainedAdd(map, "id", getID(), stringifer);
        map = selfContainedAdd(map, "uuid", getUUID(), stringifer);
        map = selfContainedAdd(map, "createdAt", getCreatedAt(), stringifer);
        map = selfContainedAdd(map, "updatedAt", getUpdatedAt(), stringifer);
        map = selfContainedAdd(map, "deletedAt", getDeletedAt(), stringifer);
        map = selfContainedAdd(map, "updatedBy", getUpdatedBy(), stringifer);
        map = selfContainedAdd(map, "name", getName(), stringifer);
        try {
            map.put(
                    "content",
                    stringifer.asString(
                            getContent()));
        } catch (NullPointerException e) {
        }
        return map;
    }*/
}
