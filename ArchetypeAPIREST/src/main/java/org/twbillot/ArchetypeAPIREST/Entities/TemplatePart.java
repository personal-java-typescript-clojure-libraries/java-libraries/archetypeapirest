package org.twbillot.ArchetypeAPIREST.Entities;

import org.hibernate.sql.Template;

import java.io.Serializable;
import java.util.Arrays;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@MappedSuperclass
public abstract class TemplatePart<T extends TemplateFile> implements Serializable {
    /*@Embeddable
    public class FilePartKey extends TemplateEmbeddable<T> { public FilePartKey(T file) { super(file); } }
    @Embeddable
    public static class FilePartKey implements Serializable {
        // // // // // ATTRIBUTES
        // Serial number
        public	static	final	long		serialVersionUID	= 10070L;
        // Columns
        @Column(name = "file_id", nullable = false, updatable = false)
        protected	long	fileID;
        @Column(name = "part_id", nullable = false, updatable = false)
        protected	int		partIndex;
        // Constructors
        public FilePartKey() { }
        public FilePartKey(
                TemplateEntity	file,
                int				partIndex) {
            this.fileID = file.getID();
            this.partIndex = (partIndex + 1);
        }
        // Getters
        public	long	getFileId() { return fileID; }
        public	int		getPartIndex() { return (partIndex - 1); }
        // Setters
        public	void	setFileId(long fileId) { this.fileID = fileId; }
        public	void	setFileId(TemplateEntity file) { this.fileID = file.getID(); }
        public	void	setPartIndex(int partIndex) { this.partIndex = (partIndex + 1); }
        // Overrides
        @Override
        public	boolean	equals(
                Object	other) {
            if (other == null) { return false; }
            if (other == this) { return true; }
            if (other instanceof TemplatePart.FilePartKey) {
                FilePartKey fpiOther = (FilePartKey) other;
                if (fpiOther.fileID == fileID
                        && fpiOther.partIndex == partIndex) {
                    return true;
                }
                return false;
            }
            return false;
        }
    }
    // // // // // ATTRIBUTES
    // Serial number
    public	static	final	long		serialVersionUID	= 10060L;
    // Immutable attributes
    private	static	final	int			maxSize				= initMaxSize();
    // Table Columns:
    // Identifier Columns
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private		long		id;
    @Embedded
    private		FilePartKey	key;
    @Lob
    @NotNull
    @Size(max = 65535)
    private		byte[]		content;
    // // // // // CONSTRUCTORS
    public TemplatePart(
            T		file,
            int		partIndex,
            byte[]	part) {
        setKey(file, partIndex);
        setContent(part);
    }
    // // // // // GETTERS
    public	static	final	int	getMaxSize() { return maxSize; }
    public	long			getId() { return id; }
    public	FilePartKey		getKey() { return key; }
    public	byte[]			getContent() { return content; }
    // // // // // SETTERS
    public	void	setId(long id) { this.id = id; }
    public	void	setKey(FilePartKey key) { this.key = key; }
    public	void	setKey(
            T	file,
            int	partIndex) {
        setKey(
                new FilePartKey(
                        file,
                        partIndex));
    }
    public	void	setContent(byte[] part) { this.content = part; }
    // Immutable Methods
    private	static	final	int	initMaxSize() {
        return 65535;
    }
    // Override Methods
    @Override
    public	boolean	equals(
            Object	other) {
        if (other == null) { return false; }
        if (other == this ) { return true; }
        if (other instanceof TemplatePart) {
            @SuppressWarnings("unchecked")
            TemplatePart<T> otherPart = (TemplatePart<T>) other;
            return otherPart.id == id
                    && otherPart.key.equals(key)
                    && Arrays.equals(otherPart.content, content);
        }
        return false;
    }*/
}
